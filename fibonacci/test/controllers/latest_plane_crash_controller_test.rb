require 'test_helper'

class LatestPlanceCrashControllerTest < ActionDispatch::IntegrationTest
  def test_index
    get "/index"
    assert_equal 200, status
    assert_equal({ count: 5702887 }, response.parsed_body)
  end
end
