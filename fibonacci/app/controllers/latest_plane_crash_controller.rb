class LatestPlaneCrashController < ApplicationController
	caches_action :index, expires_in: 1.hour

	def index
		render :json => { count: calculate_fibonacci(34)}
	end

	private

	def calculate_fibonacci(number)
		return 1 if number <= 2
		recursive_fibonacci(3, number, 1, 1)
	end

	def recursive_fibonacci(current, goal, number1, number2)
		result = number1 + number2

		return result if current == goal

		recursive_fibonacci(current+1, goal, number2, result)
	end

end